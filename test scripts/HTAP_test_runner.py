import os
import argparse
import random
import time
import csv

# run case: python3 -u row_col_test_runner.py --runner=../cmake-build-release/db_test3
# result merge: use grep "data_num_," results/*/out.txt -A 6 -h
# run manual : ./a | unbuffer -p tee output.txt
# local simple test python3 -u HTAP_test_runner.py --runner=../cmake-build-release/HTAP_test --test_type=0
KB = 1024
MB = 1024 * 1024


# memtable
parser = argparse.ArgumentParser(description='db test runner')
parser.add_argument('--runner', default="HTAP_test")
parser.add_argument('--case_from', default="")
parser.add_argument('--test_type', default=1)
args = parser.parse_args()
runner_relative_path = os.path.abspath(args.runner)
case_from = args.case_from
test_type = int(args.test_type)
# print(test_type)
start_test = False

if case_from == "":
    start_test = True


def exe_cmd(cmd):
    p = os.popen(cmd)
    p.read()
    exit_status = p.close()
    if exit_status is None:
        return 0
    else:
        return os.WEXITSTATUS(exit_status)


def test(data_num, col_num, max_file_size, block_cache_size, rounds, col_get, num_frac, query_rounds, write_num,  cmp,  advisor_addr):
    global runner_relative_path
    global start_test
    global case_from
    global rest_secs
    name = "d%d_c%d_f%d_b%d_r%d_cg%d_nf%d_q%d_wr%d_cmp%d_%s" % (data_num, col_num, max_file_size, block_cache_size, rounds, col_get, int(num_frac*10), query_rounds, write_num, cmp,"a" if len(advisor_addr) > 0 else "na")
    if not start_test:
        if name == case_from:
            start_test = True
        else:
            print("ignore %s" % name);

    cmpcvt = ""
    if len(advisor_addr) > 0:
            cmpcvt = "--conversion_=1 --compaction_=1"
    if block_cache_size == 0:
        fill_cache = "--fill_cache_=0"


    if start_test:
        # build workloads
        workloads_str = "";
        for i in range(write_num-1):
            workloads_str += "0"; #insert
        for i in range(10 - write_num):
            workloads_str += str( (i+1) % 3 + 1) #three kinds of query
        workloads_str = '0' + ''.join(random.sample(workloads_str,len(workloads_str)))

        print("\ntesting.. %s" % name)


        max_file_size = max_file_size * MB
        write_buffer_size = max_file_size
        block_cache_size = block_cache_size * MB

        cmd = "cd HTAP_results/%s; %s --data_num_=%d --col_num_=%d --write_buffer_size_=%d --block_cache_size_=%d   --advisor_addr_=%s %s --HTAP_rounds=%d --HTAP_query_rounds=%d  --htap_workloads=%s   --col_get=%d --num_frac=%.1f --compression=%d --gtest_filter=HTAP_TEST.TestHTAP --LO_num_limit_=2 > out.txt" \
              % (name, runner_relative_path, data_num, col_num, write_buffer_size, block_cache_size,
                 advisor_addr, cmpcvt, rounds, query_rounds, workloads_str, col_get, num_frac, cmp)

        print(cmd)
        exe_cmd("mkdir -p HTAP_results/%s" % name)
        start = time.time()
        if exe_cmd(cmd) != 0:
            print("Error!!!!")
        end = time.time()



        with open("htap_result.csv", 'a+') as outcsv:
            #configure writer to write standard csv file
            writer = csv.writer(outcsv, dialect=None, quoting=csv.QUOTE_NONE, quotechar='', escapechar=' ')
            # writer.writerow(['number', 'text', 'number'])
            with open("HTAP_results/%s/out.txt" % (name), 'r') as test_out:
                line = test_out.readline()
                round_result = "";
                while (line):
                    if line.startswith("summary,"):
                        round_result = line[len("summary,"):-1]
                        break;
                    line = test_out.readline()
            #Write item to outcsv
                writer.writerow([name, data_num, col_num, max_file_size / MB, block_cache_size / MB, rounds, col_get, num_frac, query_rounds, write_num, cmp, advisor_addr, end-start, round_result]);

max_rounds = 10

with open("htap_result.csv", 'w') as outcsv:
    writer = csv.writer(outcsv, dialect=None, quoting=csv.QUOTE_NONE, quotechar='', escapechar=' ')
    writer.writerow(["name", "data_num", "col_num", "max_file_size", "block_cache_size", "rounds" , "col_get" , "num_frac", "query_rounds", "write_num", "cmp", "advisor_addr", "script_time", "db_size", "time_after_round3", "total_time", "each_rounds"]);




# test
if test_type == 0:
    for data_num in [10000]:
        for col_num in [10]:
            for max_file_size in [32]:
                for block_cache_size in [0]:
                    for rounds in [5]:
                        for col_get in [1]:
                            for num_frac in [0.2]:
                                for query_rounds in [10]:
                                    # each round 10 query
                                    for write_num in [1, 5]:
                                        for cmp in [0, 7]:
                                            for advisor_addr in ["a", ""]:
                                                test(data_num, col_num, max_file_size, block_cache_size, rounds, col_get, num_frac, query_rounds, write_num, cmp, advisor_addr)


else:
    for data_num in [100000]:
        for col_num in [20, 60]: #10 30 60
            for max_file_size in [32]:
                for block_cache_size in [16]:
                    for rounds in [8]:
                        for col_get in [1, 3, 60]: #[1, 3, 60]
                            for num_frac in [0.2, 0.8]:  #0.5
                                for query_rounds in [30]:
                                    # each round 10 query
                                    for write_num in [2, 8]:  # [2, 5, 8]
                                        for cmp in [0]:    #"compression type: 0 - no compression, 1- snappy, 4- lz4, 7-zstd "
                                            for advisor_addr in ["a", ""]:
                                                test(data_num, col_num, max_file_size, block_cache_size, rounds, col_get, num_frac, query_rounds, write_num, cmp, advisor_addr)

# TEST modify


