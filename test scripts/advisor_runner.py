import os

def exe_cmd(cmd):
    p = os.popen(cmd)
    p.read()
    exit_status = p.close()
    if exit_status is None:
        return 0
    else:
        return os.WEXITSTATUS(exit_status)
# if exe_cmd() != 0:

s = exe_cmd("cd 1000W_wwr_na; ../db_test3 --data_num_=10000000 --col_num_=30  --batch_num_=400000 --default_block_type_=1 --gtest_filter=DBTest.TestAdvisor --advisor_addr_=  --LO_num_limit_=2 > col_30_l02.txt");
if s!= 0:
    print("Error");

s = exe_cmd("cd 1000W_wwr_a; ../db_test3 --data_num_=10000000 --col_num_=30  --batch_num_=400000 --default_block_type_=1 --gtest_filter=DBTest.TestAdvisor  --converison_=1 --compaction_=1 --LO_num_limit_=2 > col_30_l02.txt");
if s!= 0:
    print("Error");