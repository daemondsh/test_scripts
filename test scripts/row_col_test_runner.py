import os
import argparse

# run case: python3 -u row_col_test_runner.py --runner=../cmake-build-release/db_test3
# result merge: use grep "data_num_," results/*/out.txt -A 6 -h
# run manual : ./a | unbuffer -p tee output.txt
# local simple test python3 -u row_col_test_runner.py --runner=../cmake-build-debug/db_test3 --test_type=0 --rest_secs=0
KB = 1024
MB = 1024 * 1024

# memtable
parser = argparse.ArgumentParser(description='db test runner')
parser.add_argument('--runner', default="db_test3")
parser.add_argument('--case_from', default="")
parser.add_argument('--rest_secs', default=5)
parser.add_argument('--test_type', default=1)
parser.add_argument('--reuse_db', default=1)
args = parser.parse_args()
runner_relative_path = os.path.abspath(args.runner)
case_from = args.case_from
rest_secs = int(args.rest_secs)
reuse_db = args.reuse_db
test_type = int(args.test_type)
# print(test_type)
start_test = False

if case_from == "":
    start_test = True


def exe_cmd(cmd):
    p = os.popen(cmd)
    p.read()
    exit_status = p.close()
    if exit_status is None:
        return 0
    else:
        return os.WEXITSTATUS(exit_status)


def test(data_num, col_num, value_col_length, write_buffer_size, block_cache_size, refresh):
    global runner_relative_path
    global start_test
    global case_from
    global rest_secs
    name = "d%s_c%s_v%s_w%s_b%s" % (data_num, col_num, value_col_length, write_buffer_size, block_cache_size)
    if not start_test:
        if name == case_from:
            start_test = True
        else:
            print("ignore %s" % name);

    if start_test:
        print("\ntesting.. %s" % name)

        # os.popen("cd results/%s; echo ss > ss.txt" % name).read()

        write_buffer_size = write_buffer_size * MB
        block_cache_size = block_cache_size * MB

        cmd = "cd results/%s; %s --data_num_=%d --col_num_=%d --value_col_length_=%d --write_buffer_size_=%d --block_cache_size_=%d  --gtest_filter=DBTest.RowColPerformanceTest --rest_secs_=%d --refresh_=%d --result_file=../result.csv --advisor_addr_=  > out.txt" \
              % (name, runner_relative_path, data_num, col_num, value_col_length, write_buffer_size, block_cache_size,
                 rest_secs, refresh)
        print(cmd)
        exe_cmd("mkdir -p results/%s" % name)
        if exe_cmd(cmd) != 0:
            print("Error!!!!")

# test
if test_type == 0:
    for data_num in [10000, 50000]:
        for col_num in [10]:
            for value_col_length in [4]:
                for write_buffer_size in [8]:  # MB
                    refresh = 1
                    for block_cache_size in [8, 32]:  # MB
                        test(data_num, col_num, value_col_length, write_buffer_size, block_cache_size, refresh)
                        if reuse_db:
                            refresh = 0
else:
    for data_num in [10000000, 30000000, 60000000]:
        for col_num in [10, 50, 100, 150]:
            for value_col_length in [4]:
                for write_buffer_size in [64]:  # MB [8, 64]:
                    refresh = 1
                    for block_cache_size in [16, 1024]:  # MB
                        test(data_num, col_num, value_col_length, write_buffer_size, block_cache_size, refresh)
                        if reuse_db:
                            refresh = 0
