import os
import argparse

# run case: python3 -u ./tpcds_test_runner.py --runner=../cmake-build-release/tpcds_test | unbuffer -p tee runner.log
# local simple test python3 /Users/daemondshu/Documents/Code/C/rocksdb/helper/tpcds_test_runner.py --runner=../cmake-build-debug/tpcds_test --test_type=0
KB = 1024
MB = 1024 * 1024

# memtable
parser = argparse.ArgumentParser(description='db test runner')
parser.add_argument('--runner', default="tpcds_test")
parser.add_argument('--case_from', default="")
parser.add_argument('--rest_secs', default=5)
parser.add_argument('--test_type', default=1)
parser.add_argument('--reuse_db', default=1)
parser.add_argument('--data_path', default="../../handled");
parser.add_argument('--query_path', default="../../log");
parser.add_argument('--col_path', default="../helper/col.json");
args = parser.parse_args()
runner_relative_path = os.path.abspath(args.runner)
data_path = os.path.abspath(args.data_path)
query_path = os.path.abspath(args.query_path)
col_path = os.path.abspath(args.col_path)
case_from = args.case_from
rest_secs = int(args.rest_secs)
reuse_db = args.reuse_db
test_type = int(args.test_type)
# print(test_type)
start_test = False

if data_path[-1] != '/':
    data_path = data_path + '/'
if query_path[-1] != '/':
    query_path = query_path + '/'

if case_from == "":
    start_test = True


def exe_cmd(cmd):
    p = os.popen(cmd)
    p.read()
    exit_status = p.close()
    if exit_status is None:
        return 0
    else:
        return os.WEXITSTATUS(exit_status)


def test(extra_col_num, max_file_size_, block_cache_size, advisor_addr, refresh, rounds, querys, conversion_each_query, compression):
    global runner_relative_path
    global start_test
    global data_path
    global query_path
    global col_path

    name = "ec%d_f%d_b%d_%s_r%d_ceq%d_cnp%d_q%d-%d" % (extra_col_num, max_file_size_, block_cache_size, "a" if len(advisor_addr) > 0 else "na", rounds, conversion_each_query, compression, querys[0], querys[1]);
    cmpcvt = ""
    fill_cache = ""
    if len(advisor_addr) > 0:
        cmpcvt = "--conversion_=1 --compaction_=1"
    if block_cache_size == 0:
        fill_cache = "--fill_cache_=0"
    if not start_test:
        if name == case_from:
            start_test = True
        else:
            print("ignore %s" % name);

    if start_test:
        print("\ntesting.. %s" % name)

        # os.popen("cd results/%s; echo ss > ss.txt" % name).read()
        max_file_size_ = max_file_size_ * MB
        write_buffer_size = max_file_size_
        # write_buffer_size = write_buffer_size * MB
        block_cache_size = block_cache_size * MB

        cmd = "cd results/%s; %s --extra_col_num=%d --write_buffer_size_=%d --LO_num_limit_=1 --max_file_size_=%d --block_cache_size_=%d  --advisor_addr_=%s --refresh_=%d --gtest_filter=TpcdsTest.query:TpcdsTest --data_path_=%s --query_log_path=%s --query_rounds=%d --query_from=%d --query_to=%d  --col_path=%s %s --conversion_each_query=%d %s --compression=%d > out.txt" \
              % (name, runner_relative_path, extra_col_num, write_buffer_size, max_file_size_, block_cache_size, advisor_addr,
                 refresh, data_path, query_path, rounds, querys[0], querys[1], col_path, cmpcvt, conversion_each_query, fill_cache, compression)
        print(cmd)
        exe_cmd("mkdir -p results/%s" % name)
        if exe_cmd(cmd) != 0:
            print("Error!!!!")

# test
if test_type == 0:
    for extra_col_num in [0]:
        for rounds in [20]:  # MB [8, 64]:
            for max_file_size_ in [32]:
                # max_file_size_ = write_buffer_size
                for block_cache_size in [16]:  # MB
                    for advisor_addr in ["local", ""]:
                        for querys in [(6,6)]:
                            refresh = 1
                            test(extra_col_num, max_file_size_, block_cache_size, advisor_addr, refresh, rounds, querys)
                            if reuse_db:
                                refresh = 0
else:
    # for data_num in [10000000, 30000000]:
    for extra_col_num in [0, 30]:
        for rounds in [10]:  # MB [8, 64]:
            for max_file_size_ in [16]: #32
                # max_file_size_ = write_buffer_size
                for block_cache_size in [0]:  # MB
                    for advisor_addr in ["local", ""]:
                        for conversion_each_query in [1]:
                            for compression in [4, 7]:  # 4-lz4, 7-zstd, 0-no compression
                                for querys in [(-1, -1), (0, 60)]:
                                # for querys in [(0,9),(10,19),(20,29),(30,50)]:
                                    refresh = 1
                                    test(extra_col_num, max_file_size_, block_cache_size, advisor_addr, refresh, rounds, querys, conversion_each_query, compression)
                                    if reuse_db:
                                        refresh = 0
