
#include <fcntl.h>
#include <algorithm>
#include <set>
#include <thread>
#include <unordered_set>
#include <utility>

#ifndef OS_WIN

#include <unistd.h>

#endif
#ifdef OS_SOLARIS
#include <alloca.h>
#endif

#include "cache/lru_cache.h"
#include "db/db_impl.h"
#include "db/db_test_util.h"
#include "db/dbformat.h"
#include "db/job_context.h"
#include "db/version_set.h"
#include "db/write_batch_internal.h"
#include "env/mock_env.h"
#include "memtable/hash_linklist_rep.h"
#include "monitoring/thread_status_util.h"
#include "port/port.h"
#include "port/stack_trace.h"
#include "rocksdb/cache.h"
#include "rocksdb/compaction_filter.h"
#include "rocksdb/convenience.h"
#include "rocksdb/db.h"
#include "rocksdb/env.h"
#include "rocksdb/experimental.h"
#include "rocksdb/filter_policy.h"
#include "rocksdb/options.h"
#include "rocksdb/perf_context.h"
#include "rocksdb/slice.h"
#include "rocksdb/slice_transform.h"
#include "rocksdb/snapshot.h"
#include "rocksdb/table.h"
#include "rocksdb/table_properties.h"
#include "rocksdb/thread_status.h"
#include "rocksdb/utilities/checkpoint.h"
#include "rocksdb/utilities/optimistic_transaction_db.h"
#include "rocksdb/utilities/write_batch_with_index.h"
#include "table/block_based_table_factory.h"
#include "table/mock_table.h"
#include "table/plain_table_factory.h"
#include "table/scoped_arena_iterator.h"
#include "util/compression.h"
#include "util/file_reader_writer.h"
#include "util/filename.h"
#include "util/mutexlock.h"
#include "util/rate_limiter.h"
#include "util/string_util.h"
#include "util/sync_point.h"
#include <unistd.h>
//#include "util/testharness.h"
#include "util/testutil.h"
#include "utilities/merge_operators.h"
#include "table/myrocks_schema.h"
#include "util/gflags_compat.h"
#include "util/datablock_type_advisor.h"
#include "util/stderr_logger.h"
#include "util/auto_roll_logger.h"
#include "util/get_mem.h"
#include <sstream>
#include <fstream>

#ifdef PROFILE
#include <gperftools/profiler.h>
#endif
//using GFLAGS_NAMESPACE::ParseCommandLineFlags;
#define KB 1024
#define MB (KB*KB)
//define unsigned may raise compile bug in test machine
DEFINE_int32(data_num_, 100000, "Number of data");
DEFINE_int32(col_num_, 3, "Number of column");
DEFINE_int32(col_num_gap, 0, "col get num add per iterator, 0 - no iterator");
DEFINE_int32(value_col_length_, 4, "length of each column");
DEFINE_int64(write_buffer_size_, 64 << 20,
             "64MB Amount of data to build up in memory before converting to a sorted on-disk file. (Memtable size)");
DEFINE_int64(block_size_, 4 * KB, "size of data block");
DEFINE_int64(block_cache_size_, 8 * KB * KB, "size of block cache, default 8MB");
DEFINE_string(db_name_, "/tmp/rocksdb_simple_example", "data path");
DEFINE_string(oper_name_, "", "row_all/rowgroup_all/rowgroup_single");
DEFINE_bool(delete_all_, false, "delete all data after read operation");
DEFINE_int32(LO_num_limit_, 5, "level 0 max file num"); // compatibility for some gflags without uint32
DEFINE_uint64(level_bytes_base_, 256 * KB * KB, "256M for level0 ");
DEFINE_uint64(level_multi_, 10, "level multiplier");
DEFINE_uint64(max_file_size_, 64*KB*KB, "max file size of compaction output");
DEFINE_bool(sync_, true, "writeoption.sync");
DEFINE_int32(compression, 0, "compression type: 0 - no compression, 1- snappy, 4- lz4, 7-zstd ");

// for adviosr
DEFINE_string(advisor_addr_, "localhost:50051", "empty - not use advisor_addr /  counter - use expr version");
DEFINE_bool(conversion_, false, "enable conversion advice");
DEFINE_bool(compaction_, false, "enable compaction advice");
DEFINE_int32(batch_num_, 10000, "num gap");
DEFINE_double(write_cost_multi, 3, "write cost is 3x of read cost on same size ");

DEFINE_int32(default_block_type_, 1, "1- row, 2 -col");
//DEFINE_string(result_file_, "result.csv", "outinfo to result.csv");
//for row col performance test
DEFINE_bool(refresh_, true, "delete and reload data");
DEFINE_int32(rest_secs_, 1, "sleep before start");
DEFINE_bool(path_with_type_, true, "");

DEFINE_bool(iter_reset_, true, "");
DEFINE_int32(scan_rounds_, 80, "");
DEFINE_bool(fill_cache_, true, "readoption.fill_cache");
DEFINE_string(result_file, "result.csv", "print result into a csv");

//for testadvisor
DEFINE_bool(write_with_read, true, "write_with_read in advisor,if false, read create after write");
DEFINE_bool(reopen_each_oper, true, "reopen db before operation and close db after operation");

//fot htaptest
DEFINE_string(htap_workloads, "0,1,2,3,4,1", "do these HTAP workloads these queries ");
//DEFINE_double(col_frac, 0.1, "fraction of columns get");
DEFINE_double(col_get, 1, "number of columns get");
DEFINE_double(num_frac, 0.5, "fraction of rows get");
DEFINE_int32(HTAP_rounds, 5, "rounds of HTAP workloads");
DEFINE_int32(HTAP_MAX_KEY, 20000000, "max key in HTAP workloads");
DEFINE_int32(HTAP_query_rounds, 20, "max key in HTAP workloads");

namespace rocksdb {

struct TestResult {
  std::string name;
  std::vector<float> result;
};

//  template<typename ... Args>
//  std::string string_format( const std::string& format, Args ... args )
//  {
//    size_t size = snprintf( nullptr, 0, format.c_str(), args ... ) + 1; // Extra space for '\0'
//    unique_ptr<char[]> buf( new char[ size ] );
//    snprintf( buf.get(), size, format.c_str(), args ... );
//    return std::string( buf.get(), buf.get() + size - 1 ); // We don't want the '\0' inside
//  }


std::string FLAGS_TITLE() {
  return (string_format("%15s,%8s,%10s,%15s,%15s,%15s,%15s,%15s,%15s,%10s,%10s",
                        "data_num_", "col_num", "ColLength",
                        "writeBuffSize", "block_size", "blockCacheSize", "levelBytesBase",
                        "level_multi", "max_file_size", "default_type", "conversion"));
}

std::string FLAGS_STR() {
  return (string_format("%15d,%8d,%10d,%15llu,%15llu,%15llu,%15llu,%15llu,%15llu,%10d,%10d", FLAGS_data_num_,
                        FLAGS_col_num_, FLAGS_value_col_length_,
                        FLAGS_write_buffer_size_ / MB, FLAGS_block_size_, FLAGS_block_cache_size_ / MB,
                        FLAGS_level_bytes_base_ / MB, FLAGS_level_multi_, FLAGS_max_file_size_ / MB,
                        FLAGS_default_block_type_, FLAGS_conversion_));
}



std::string option_summary() {

  return FLAGS_TITLE() + "\n" + FLAGS_STR() + "\n";
}

class DBTest : public testing::Test {
public:
//  DBTest() : DBTestBase("/db_test") {}
//  std::string &kDBName = FLAGS_db_name_;
  //std::string kDBName = "/Volumes/Darkness_Y/rocksdb_simple_example";
  std::vector<std::string> random_prefix;


  std::stringstream csv_title;
  std::stringstream csv_rows;
//  std::vector<TestResult> testResults;


  std::string getPerformanceResultsSummary(TestResult r) {
    return string_format("%10s,%8.2f,%8.2f,%8.2f\n", r.name.c_str(), r.result[0], r.result[1], r.result[2]);
  }


  std::string getPerformanceResultsSummary(std::vector<TestResult> test_results) {
    std::string summary("");
    summary += string_format("%20s,%10s,%10s,%10s\n", "name", "put", "get", "scan");
    for (size_t i = 0; i < test_results.size(); i++) {
      TestResult &r = test_results[i];
      if (r.result.size() >= 3)
        summary += string_format("%20s,%10.2f,%10.2f,%10.2f\n", r.name.c_str(), r.result[0], r.result[1],
                                 r.result[2]);
    }
    return summary;
  }

  std::string getAdvisorResultsSummary(std::vector<TestResult> test_results) {

    std::string summary("SUMMARY\n");

    summary += string_format("%20s,%10s,%10s\n", "oper", "async_time", "sync_time");
    std::map<std::string, std::vector<float>> counter;
    for (size_t i = 0; i < test_results.size(); i++) {
      TestResult &r = test_results[i];
//      if (r.result.size() >= 3)
      if (r.result.size() >= 2)
      {
        summary += string_format("%20s,%10.2f,%10.2f\n", r.name.c_str(), r.result[0], r.result[1]);
        counter[std::to_string(0) + r.name].push_back(r.result[0]);
        counter[std::to_string(1) + r.name ].push_back(r.result[1]);
      }
    }
    std::string summary2;
    for (auto iter = counter.begin(); iter!= counter.end(); iter++ )
    {
      summary2 += iter->first + ",";
      std::vector<float>& results = iter->second;
      float avg = 0;
      for (size_t i =0; i < results.size(); i++)
      {
        summary2 += std::to_string(results[i]) + ",";
        avg += results[i];
      }
      if (results.size() != 0 ) avg /= results.size();
      summary2 += "avg," + std::to_string(avg) + "\n";
    }
//    return summary+summary2;


    std::fstream csvWriter;
    csvWriter.open(FLAGS_result_file, std::ios::app);

    csvWriter << FLAGS_TITLE() << std::endl;
    csvWriter << FLAGS_STR() << std::endl;

    csvWriter << summary2 << std::endl;
    csvWriter.close();

    return summary+summary2;
  }

  TestResult PerformanceTestBody(Schema*schema, DataBlockType dataBlockType,
                                 const ReadOptions &readOptions, const std::string &kDBName,
                                 int num_records, const std::string &value, const std::vector<std::string> &prefix,
                                 const std::string profile_name, bool refresh = true);


};

void check(bool condition,std::string s)
{
  if (condition == false)
  {
    std::cout << s << std::endl;
    exit(1);
  }
}

void checkStatus(Status s) {
  if (!s.ok()) {
    std::cout << s.ToString() << std::endl;
    exit(1);
  }
}

void WaitForDBBackJobsAndClose(DB* db, bool close = true, ColumnFamilyHandle* cf= nullptr)
{
  //todo try cancel some memory table
  if (cf != nullptr) {
    dynamic_cast<DBImpl*>(db)->Flush(FlushOptions(), cf);
  }
  dynamic_cast<DBImpl*>(db)->TEST_WaitForFlushMemTable();
  dynamic_cast<DBImpl*>(db)->TEST_WaitForCompact(true);
  if (close)
  {
    db->Close();
    delete db;
  }

}

Options getDefaultOption(Schema*schema = nullptr, std::shared_ptr<Cache> shared_cache = nullptr, std::string log_name ="advisor.log", DataBlockType default_data_block_type = DataBlockType::Unknown) {
  Options options;
//  options.compression_per_level
  options.compression = static_cast<CompressionType>(FLAGS_compression);
  options.info_log.reset(new FileLogger(InfoLogLevel::INFO_LEVEL, "rocksdb_log.txt"));
  // Optimize RocksDB. This is the easiest way to get RocksDB to perform well
  options.IncreaseParallelism();
//  options.max_background_jobs = 10;
//  options.OptimizeLevelStyleCompaction();
  // create the DB if it's not already present
  options.create_if_missing = true;
  options.write_buffer_size = FLAGS_write_buffer_size_;
  options.level0_file_num_compaction_trigger = FLAGS_LO_num_limit_;
  options.max_bytes_for_level_base = FLAGS_level_bytes_base_;
  options.max_bytes_for_level_multiplier = FLAGS_level_multi_;
  options.target_file_size_base = FLAGS_max_file_size_;

  options.default_datablock_type = static_cast<DataBlockType>(default_data_block_type == DataBlockType::Unknown
                                                              ? FLAGS_default_block_type_ : default_data_block_type);


//  ((BlockBasedTableOptions*)options.table_factory->GetOptions())->data_block_type = DataBlockType::RowGroupBased;
  ((BlockBasedTableOptions*) options.table_factory->GetOptions())->schema = schema;
  ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_size = FLAGS_block_size_;

  if (shared_cache!= nullptr)
    ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache = shared_cache;
  else
    ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache = NewLRUCache(
        FLAGS_block_cache_size_);

  ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache->SetStrictCapacityLimit(true);
  ((BlockBasedTableOptions*) options.table_factory->GetOptions())
      ->flush_block_policy_factory = std::make_shared<FlushBlockBySizePolicyFactory>();

  int restart_interval = ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_restart_interval;
  if (FLAGS_advisor_addr_.size() > 0)
    options.dataBlockTypeAdvisor = new DataBlockTypeAdvisor(schema, FLAGS_advisor_addr_,FLAGS_compaction_,FLAGS_conversion_,FLAGS_block_size_,log_name,restart_interval, FLAGS_write_cost_multi);
//    ((BlockBasedTableOptions *) options.table_factory->GetOptions())->
  return options;
}



std::string GetDBInfo(std::string db_name, const Options& options, DB* db = nullptr, bool hex = false, int column_family_id=-1)
{
  std::string r;
  if (db == nullptr)
  {
    Status s = DB::Open(options,db_name, &db);
    checkStatus(s);
    r = dynamic_cast<DBImpl*>(db)->GetSuperVersionDebugString(hex, column_family_id);

    WaitForDBBackJobsAndClose(db);
  } else
  {
    r = dynamic_cast<DBImpl*>(db)->GetSuperVersionDebugString(hex, column_family_id);
  }
  return r;
}


std::string RandomString(Random*rnd, int len) {
  std::string r;
  test::RandomString(rnd, len, &r);
  return r;
}

std::vector<std::string> RandomStrings(uint32_t n, int len, Random*rnd = nullptr)
{
  Random* rnd_temp = nullptr;
  if (rnd == nullptr)
  {
    rnd_temp = new Random(100);
    rnd = rnd_temp;
  }
  std::vector<std::string> result;
  result.clear();
  result.reserve(n);
  for (uint32_t i = 0; i < n; ++i) {
    std::string temp = RandomString(rnd, len);
    while (temp.find(",") != std::string::npos) //for csv
      temp = RandomString(rnd, len);
    result.emplace_back(temp);
  }
  if (rnd_temp != nullptr)
  {
    delete rnd_temp;
  }
  return result;
}

std::string GenerateKey(int primary_key, int secondary_key, int padding_size,
                        Random*rnd) {
  char buf[50];
  char*p = &buf[0];
  snprintf(buf, sizeof(buf), "%7d%3d", primary_key, secondary_key);
  std::string k(p);
  if (padding_size) {
    k += RandomString(rnd, padding_size);
  }

  return k;
}

std::string GenerateValues(const std::vector<uint32_t> &lengths, Schema*schema, Random*rnd, bool have_null = false) {
  std::vector<Slice> values;
  std::vector<std::string> values_set;
  size_t n = schema->GetColsNum();
  values.reserve(n);
  values_set.reserve(n);
  for (size_t i = 0; i < n; ++i) {
    if (have_null && rnd->Uniform(6) > 4) {
      values_set.emplace_back("");
    } else
      values_set.emplace_back(RandomString(rnd, lengths[i]));
    values.emplace_back(values_set[i]);
  }
  std::string ans;
  schema->BuildValueSlice(values.data(), &ans, nullptr);
  return ans;
}


std::string GenerateSampleValue(Schema*schema, Random*rnd) {
  std::vector<Slice> values;
  size_t n = schema->GetColsNum();
  values.reserve(n);
  for (size_t i = 0; i < n; ++i) {
    values.emplace_back(RandomString(rnd, schema->GetColLength(i)));
  }
  std::string ans;
  schema->BuildValueSlice(values.data(), &ans, nullptr);
  return ans;
}

// Generate random key value pairs.
// The generated key will be sorted. You can tune the parameters to generated
// different kinds of test key/value pairs for different scenario.
void GenerateRandomKVs(std::vector<std::string>*keys,
                       std::vector<std::string>*values, const int from,
                       const int len, const std::vector<uint32_t> &lengths, Schema*schema,
                       Random rnd = Random(300),
                       const int step = 1,
                       const int padding_size = 0,
                       const int keys_share_prefix = 1) {
  // generate different prefix
  for (int i = from; i < from + len; i += step) {
    // generating keys that shares the prefix
    for (int j = 0; j < keys_share_prefix; ++j) {
      keys->emplace_back(GenerateKey(i, j, padding_size, &rnd));
      values->emplace_back(GenerateValues(lengths, schema, &rnd));
    }
  }
}

Schema*GenerateSchema(const std::vector<uint32_t> &lengths) {
  Schema*ans = new MyRocksSchema;
  size_t n = lengths.size();
  for (size_t i = 0; i < n; ++i) {
//    if (i % 2 == 0) {
    ans->AddFixedLenCol(lengths[i]);
//    } else {
//      ans->AddVarLenCol(lengths[i]);
//    }
  }
  ans->CommitAddCol();
  return ans;
}

std::string GetValueAtCol(std::string value, const char*bitmap, Schema*schema) {
  if (bitmap == nullptr) return value;
  size_t n = schema->GetColsNum();
  auto*values = new Slice[n];
  schema->ReadValueSlice(value.data(), value.size(), values);
  for (uint32_t i = 0; i < n; ++i) {
    if (!CheckAtBitmap(bitmap, i)) {
      values[i] = Slice();
    }
  }
  std::string ans;
  schema->BuildValueSlice(values, &ans, bitmap);
  delete[] values;
  return ans;
}

char* GenerateBitmap(int col_size, const std::vector<int> &selected_values, bool skip = false) {
  size_t byteSize = ((col_size + 7u) >> 3u);
  char*ans = new char[byteSize];
  memset((void*) ans, 0x00, byteSize);
  if (skip) {
    for (auto idx : selected_values) {
      if (idx < 0 || idx >= col_size) continue;  // skip invalid input directly
      ans[idx >> 3u] |= 1u << (idx & 0x7);
    }
  } else {
    for (int idx = 0; idx < col_size; ++idx) {
      ans[idx >> 3u] |= 1u << (idx & 0x7);
    }

    //TODO bitmap空是会报错
    for (auto idx : selected_values) {
      if (idx < 0 || idx >= col_size) continue;  // skip invalid input directly
      ans[idx >> 3u] ^= 1u << (idx & 0x7);
    }
  }
  return ans;
}

std::string GenerateStrWithPrefixes(int i, const std::vector<std::string> &prefixes) {
  char buf[50];
  char*p = &buf[0];
  snprintf(buf, sizeof(buf), "%5d", i % 10008);
  std::string str(p);
  str = prefixes[i % prefixes.size()] + str;
  return str;
}

std::string GenerateStrWithPrefixAndSuffixes(const std::string prefix, int i, const std::vector<std::string> &suffixes) {
  char buf[50];
  char*p = &buf[0];
  snprintf(buf, sizeof(buf), "%s%5d%s", prefix.c_str(), i % 10008, suffixes[i % suffixes.size()].c_str());
  std::string str(p);
  return str;
}
}

