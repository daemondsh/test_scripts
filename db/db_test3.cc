
#include "my_test.h"

namespace  rocksdb {
TestResult DBTest::PerformanceTestBody(Schema*schema, DataBlockType dataBlockType,
                               const ReadOptions &readOptions, const std::string &kDBName,
                               int num_records, const std::string &value, const std::vector<std::string> &prefix,
                               const std::string profile_name, bool refresh) {

  std::cout << getCurrentRSS() / KB / KB << std::endl;
  TestResult r;
  r.name = profile_name;
  if (FLAGS_oper_name_ != "")
    if (FLAGS_oper_name_.find(profile_name) == std::string::npos || profile_name.find(FLAGS_oper_name_) == std::string::npos) {
      std::cout << "skip test:" << profile_name << std::endl;
      return r;
    }

  std::cout << "have a rest..." << std::endl;
  sleep(static_cast<unsigned int>(FLAGS_rest_secs_));
  std::cout << profile_name << " test start:" << std::endl;

  DB*db;

  WriteOptions writeOptions = WriteOptions();
  writeOptions.sync = FLAGS_sync_;  //avoid data loss

//  assert(s.ok());
//  ASSERT_OK(s);
  StopWatchNano timer(Env::Default(), true);

  // write

  {
    if (refresh) {
      Options options = getDefaultOption(schema, nullptr, "advisor.log", dataBlockType);
      DestroyDB(kDBName, options);
      Status s = DB::Open(options, kDBName, &db);
      WriteBatch writeBatch;
      for (int i = 0; i < num_records; ++i) {

        s = writeBatch.Put( GenerateStrWithPrefixes(i, prefix), value);
        checkStatus(s);
        if (writeBatch.GetDataSize() > 4 * MB)
        {
          s = db->Write(writeOptions, &writeBatch);
          checkStatus(s);
          writeBatch.Clear();
        }
//    ASSERT_OK(s);
      }
      if (writeBatch.GetDataSize() > 0)
      {
        s = db->Write(writeOptions, &writeBatch);
        checkStatus(s);
        writeBatch.Clear();
      }

      std::cout << "try to close db... now:" << timer.ElapsedNanos(false) / pow(10,9) << std::endl;
      WaitForDBBackJobsAndClose(db, true);
      r.result.push_back(timer.ElapsedNanos(true) / pow(10, 9));
    } else {
      r.result.push_back(0);
    }
    std::cout << "Put cost: " << r.result.back() << std::endl;
  }
  // avoid compaction and memtable influence




  Options options = getDefaultOption(schema, nullptr, "advisor.log",dataBlockType);
  options.disable_auto_compactions = true;

//  options.default_datablock_type = dataBlockType;
  Status s = DB::Open(options, kDBName, &db);
  auto cfh = db->DefaultColumnFamily();

  std::cout << getCurrentRSS() / KB / KB << std::endl;
#ifdef PROFILE
  ProfilerStart((profile_name+"_get.prof").c_str());
#endif
  {
    timer.ElapsedNanos(true);
    Random rnd(100);
    int num_get = num_records / 10;
    for (int i = 0; i < num_get; ++i) {

      std::vector<PinnableSlice> actualValues(schema->GetColsNum());
      s = db->Get(readOptions, cfh, GenerateStrWithPrefixes(rnd.Uniform(num_records), prefix), actualValues.data());
      checkStatus(s);
    }
    r.result.push_back(timer.ElapsedNanos(true) / pow(10, 9));
    std::cout << "Random Get cost: " << r.result.back() << std::endl;

  }
#ifdef PROFILE
  ProfilerStop();
#endif

  //scan
#ifdef PROFILE
  ProfilerStart((profile_name+"_scan.prof").c_str());
#endif
  uint32_t sum=0;
  {
    std::cout << getCurrentRSS() / KB / KB << std::endl;

    timer.ElapsedNanos(true);
    unique_ptr<Iterator> db_iter(db->NewIterator(readOptions));

    Random rnd(200);

    for (int round = 1; round < FLAGS_scan_rounds_; round++) {
      std::string from = GenerateStrWithPrefixes(rnd.Uniform(num_records), prefix);
      uint32_t max_next = rnd.Uniform(num_records / 4);
      uint32_t count = 0;
      //todo back
      for (db_iter->Seek(from); db_iter->Valid() && count < max_next; db_iter->Next(), count++) {

//          for (db_iter->Seek(from); db_iter->Valid() && count < max_next; db_iter->Next(), count++) {
//      // Do something with it->key() and it->value().
//          std::string value1;[
        auto value1 = db_iter->values();
//      value1 = GetValueAtCol(value1, readOptions.col_bitmap, schema);
//      value2 = GetValueAtCol(values[i++], readOptions.col_bitmap, schema);
//      ASSERT_EQ(0, value1.compare(value2));
      }
      checkStatus(db_iter->status());
      sum += count;
//        std::cout << getCurrentRSS() / KB / KB << std::endl;
    }
    //check scan cost when iterator not reset
    r.result.push_back(timer.ElapsedNanos(true) / pow(10, 9));
    std::cout << "Scan all cost: " << r.result.back() << "with next: " << sum << std::endl;
  }

#ifdef PROFILE
  ProfilerStop();
#endif

//  {
//    WriteBatch batch;
//    for (int i = 0; i < num_records; i += 2) {
//      s = batch.Delete(keys[i]);
////      assert(s.ok());
//    }
//    s = db->Write(writeOptions, &batch);
////    assert(s.ok());
//  }
//
//  std::cout << "Batch Delete cost: " << timer.ElapsedNanos(true) / pow(10, 9) << std::endl;
//
//  //NOTE: here failed
  if (FLAGS_delete_all_) {
    for (int i = 1; i < num_records; i += 1) {
      s = db->Delete(WriteOptions(), nullptr, GenerateStrWithPrefixes(i, prefix));
      checkStatus(s);
    }
  }
//  }
//  std::cout << "Normal Delete cost: " << timer.ElapsedNanos(true) << std::endl;*/
//
//  for (int i = 0; i < num_records; i += 2) {
//    std::string value;
//    s = db->Get(readOptions, keys[i], &value);
//    assert(s.IsNotFound());
//  }
  std::cout << "block usage: "
            << ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache->GetUsage() << std::endl;


  db->Close();
  delete db;

  {
    Options options_temp = getDefaultOption(schema, nullptr, "advisor.log", dataBlockType);
    Status s2 = DB::Open(options_temp, kDBName, &db);
    std::cout << dynamic_cast<DBImpl*>(db)->GetSuperVersionDebugString() << std::endl;
    db->Close();
    delete db;
  }
  std::cout << getCurrentRSS() / KB / KB << std::endl;


  for (size_t i=0; i < r.result.size(); i++)
    csv_rows << std::fixed << std::setw(9) << std::setprecision(2) << r.result[i] << ",";
  return r;
}







//TODO this test is out of data
TEST_F(DBTest, SimpleTestDBUsageRowBased) {

  DB*db;
  Options options;
  Schema*schema = GenerateSchema({20});

  // Optimize RocksDB. This is the easiest way to get RocksDB to perform well
  options.IncreaseParallelism();
//  options.OptimizeLevelStyleCompaction();
  // create the DB if it's not already present
  options.create_if_missing = true;

  ((BlockBasedTableOptions*) options.table_factory->GetOptions())->schema = schema;

  DestroyDB(FLAGS_db_name_, options);
  Status s = DB::Open(options, FLAGS_db_name_, &db);
  ASSERT_OK(s);

  // Put key-value
  s = db->Put(WriteOptions(), "key1", "value");
  ASSERT_OK(s);
  std::string value;

  // get value
  s = db->Get(ReadOptions(), "key1", &value);
  ASSERT_OK(s);
  ASSERT_EQ("value", value);

  //scan first value
  {
    unique_ptr<Iterator> db_iter(db->NewIterator(ReadOptions()));
    db_iter->SeekToFirst();
    ASSERT_TRUE(db_iter->Valid());
    ASSERT_EQ("value", db_iter->value());
    db_iter->Next();
    ASSERT_FALSE(db_iter->Valid());
  }


  // atomically apply a set of updates
  {
    WriteBatch batch;
    batch.Delete("key1");
    batch.Put("key2", value);
    s = db->Write(WriteOptions(), &batch);
  }

  s = db->Get(ReadOptions(), "key1", &value);
  ASSERT_TRUE(s.IsNotFound());

  db->Get(ReadOptions(), "key2", &value);
  ASSERT_EQ("value", value);

  {
    PinnableSlice pinnable_val;
    db->Get(ReadOptions(), db->DefaultColumnFamily(), "key2", &pinnable_val);
    ASSERT_EQ("value", pinnable_val);
  }

  {
    std::string string_val;
    // If it cannot pin the value, it copies the value to its internal buffer.
    // The intenral buffer could be set during construction.
    PinnableSlice pinnable_val(&string_val);
    db->Get(ReadOptions(), db->DefaultColumnFamily(), "key2", &pinnable_val);
    ASSERT_EQ("value", pinnable_val);
    // If the value is not pinned, the internal buffer must have the value.
    ASSERT_TRUE(pinnable_val.IsPinned() || string_val == "value");
  }

  PinnableSlice pinnable_val;
  db->Get(ReadOptions(), db->DefaultColumnFamily(), "key1", &pinnable_val);
  ASSERT_TRUE(s.IsNotFound());
  // Reset PinnableSlice after each use and before each reuse
  pinnable_val.Reset();
  db->Get(ReadOptions(), db->DefaultColumnFamily(), "key2", &pinnable_val);
  ASSERT_EQ("value", pinnable_val);
  pinnable_val.Reset();
  // The Slice pointed by pinnable_val is not valid after this point

  db->Close();


  delete schema;
  delete db;
}
//
//void printDBDebugString(DB* db)
//{
//  DBImpl* dbimpl = dynamic_cast<DBImpl *>(db);
//
//  for (int i=0; i<100; i++)
//  {
////    dbimpl->GetColumnFamilyHandleUnlocked()
//    SuperVersion* sv = dbimpl->GetAndRefSuperVersion(0u);
//    if (sv != nullptr)
//    {
//      std::cout << "column family " << i << std::endl;
//      std::string str =  sv->current->DebugString(true, true);
//      std::cout << str << std::endl;
//    }
//  }
//
//
//}

//TEST_F(DBTest, VersionInfoGetTest){
//
//}

//Options InitOption()
////{
////
////}


// test correctness on large dataset

//suggest args: --LO_num_limit_=4 --col_num_=10 --data_num_=50000 --block_cache_size_=100000 --write_buffer_size_=0  --default_block_type_=2
TEST_F(DBTest, SimpleCaseTest) {
  if (FLAGS_data_num_ > 5000000) {
    std::cout << "simple case  not used for performance but for corrtness test" << std::endl;
    return;
  }

  std::vector<uint32_t> lengths(FLAGS_col_num_, FLAGS_value_col_length_);
  Schema* schema = GenerateSchema(lengths);
  Options options = getDefaultOption(schema);

  WriteOptions writeOptions;
  writeOptions.sync = FLAGS_sync_;
  ReadOptions readOptions;
  readOptions.fill_cache = FLAGS_fill_cache_;


  writeOptions.sync = true;
  char*bitmap = GenerateBitmap(schema->GetColsNum(), {1, 2});
  readOptions.SetBitmap(bitmap, schema);


  // Optimize RocksDB. This is the easiest way to get RocksDB to perform well

  std::cout << "mem: " << getCurrentRSS() / KB / KB << std::endl;
  DB*db;

//    readOptions.fill_cache = false;




  DestroyDB(FLAGS_db_name_, options);
  Status s = DB::Open(options, FLAGS_db_name_, &db);
  ASSERT_OK(s);
//  DBImpl* dbimpl = dynamic_cast<DBImpl *>(db);
//  SuperVersion* t = dbimpl->GetAndRefSuperVersion(0);
//  printDBDebugString(db);
//  std::cout<< dynamic_cast<DBImpl *>(db) ->GetSuperVersionDebugString() << std::endl;
  std::vector<std::string> keys;
  std::vector<std::string> values;
  int num_records = FLAGS_data_num_;

  GenerateRandomKVs(&keys, &values, 0, num_records, lengths, schema);
  std::cout << values[0] << std::endl;
  //put

  ASSERT_EQ(0, num_records % 2);
  int half_num_records = num_records / 2;
  WriteBatch writeBatch;
  for (int i = 0; i < half_num_records; ++i) {
    s = writeBatch.Put(keys[i], values[i]);
    ASSERT_OK(s);
    //to avoid Trivial Move compaction
    s = writeBatch.Put(keys[i+half_num_records], values[i+half_num_records]);
    ASSERT_OK(s);

    if (writeBatch.GetDataSize() > 4 * MB)
    {
      s = db->Write(writeOptions, &writeBatch);
      ASSERT_OK(s);
      writeBatch.Clear();
    }
  }

  if (writeBatch.GetDataSize() > 0)
  {
    s = db->Write(writeOptions, &writeBatch);
    ASSERT_OK(s);
    writeBatch.Clear();
  }


  std::cout << dynamic_cast<DBImpl*>(db)->GetSuperVersionDebugString() << std::endl;
  std::cout << ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache->GetUsage() << std::endl;


  auto cfh = db->DefaultColumnFamily();
  //get and check


  for (int i = 0; i < num_records; ++i) {
//      std::string value;
    std::vector<PinnableSlice> actualValues(schema->GetColsNum());
    s = db->Get(readOptions, cfh, keys[i % num_records], actualValues.data());
    ASSERT_OK(s);

//      value = GetValueAtCol(value, bitmap, schema);
    std::vector<Slice> expectValues(schema->GetColsNum());
    schema->ReadValueSlice(values[i % num_records].c_str(), values[i % num_records].size(), expectValues.data());
    for (uint32_t j = 0; j < readOptions.should_read_cnt; j++) {
      ASSERT_EQ(expectValues[readOptions.should_read_arr[j]], actualValues[readOptions.should_read_arr[j]]);
    }


  }
  std::cout << "mem: " << getCurrentRSS() / KB / KB << std::endl;
  std::cout << ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache->GetUsage() << std::endl;


  //scan all
  {
    unique_ptr<Iterator> db_iter(db->NewIterator(readOptions));
    uint32_t i = 0;
    for (db_iter->Seek(keys[i % num_records]); db_iter->Valid(); db_iter->Next()) {
      // Do something with it->key() and it->value().
      //std::string value = db_iter->value().ToString();
//      Slice key = keys[i];vi
//      Slice testvalue = db_iter->value();
      const Slice* iterActualValues = db_iter->values();

      std::vector<Slice> expectValues(schema->GetColsNum());
      //std::cout << i << " "<<db_iter->key().ToString() <<" "<< keys[i % num_records] <<" " <<values[i % num_records] << std::endl;
//      value = GetValueAtCol(value, bitmap, schema);
      schema->ReadValueSlice(values[i % num_records].c_str(), values[i % num_records].size(), expectValues.data());
      for (uint32_t j = 0; j < readOptions.should_read_cnt; j++) {
        ASSERT_EQ(expectValues[readOptions.should_read_arr[j]], iterActualValues[readOptions.should_read_arr[j]]);
      }

//        std::string key = db_iter->key().ToString();
      //ASSERT_EQ(value2, value);
//      //check value
//      Slice value = db_iter->value();
//      value = GetValueAtCol(value.ToString(), bitmap, schema);
//      std::string value2 = GetValueAtCol(values[i], bitmap, schema);
////    std::cout << value << "\t" << value2 << "\t" << value.compare(value2) << "\t" << i << std::endl;
//      ASSERT_EQ(0, value.compare(value2));

      i++;
    }
    ASSERT_OK(db_iter->status());
    ASSERT_EQ(num_records, i);
    ASSERT_FALSE(db_iter->Valid());
//      if (FLAGS_iter_reset_)
//        db_iter->Reset();
  }


  std::cout << "mem: " << getCurrentRSS() / KB / KB << std::endl;
  std::cout << ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache->GetUsage() << std::endl;


  if (writeOptions.sync == true)
    // async may raise that write delay
  {
    //delete test
    std::cout << "delete start" << std::endl;
    {
      WriteBatch batch;
      for (int i = 0; i < num_records; i += 3) {
        batch.Delete(keys[i]);
      }
      s = db->Write(writeOptions, &batch);
      ASSERT_OK(s);
    }

    for (int i = 0; i < num_records; i += 3) {
      std::string value;
      s = db->Get(readOptions, keys[i], &value);
      ASSERT_TRUE(s.IsNotFound());
    }

    {
      for (int i = 1; i < num_records; i += 3) {
        s = db->Delete(writeOptions, nullptr, keys[i]);
//      s = db->Delete(WriteOptions(), nullptr, keys[i+1]);
        ASSERT_OK(s);
      }
    }

    for (int i = 1; i < num_records; i += 3) {
      std::string value;
      s = db->Get(readOptions, keys[i], &value);
      ASSERT_TRUE(s.IsNotFound());
    }

    for (int i = 2; i < num_records; i += 3) {

      //check values
      std::vector<PinnableSlice> actualValues(schema->GetColsNum());
      s = db->Get(readOptions, cfh, keys[i % num_records], actualValues.data());
      ASSERT_OK(s);

      std::vector<Slice> expectValues(schema->GetColsNum());
      schema->ReadValueSlice(values[i % num_records].c_str(), values[i % num_records].size(), expectValues.data());
      for (uint32_t j = 0; j < readOptions.should_read_cnt; j++) {
//      assert(expectValues[readOptions.should_read_arr[j]].compare(actualValues[readOptions.should_read_arr[j]]) == 0);
        ASSERT_EQ(expectValues[readOptions.should_read_arr[j]], actualValues[readOptions.should_read_arr[j]]);
      }

    }
  }

  dynamic_cast<DBImpl*>(db)->TryConversionAndSchedule(db->DefaultColumnFamily()->GetID());
  dynamic_cast<DBImpl*>(db)->TEST_WaitForCompact(true);
//  std::cout << dynamic_cast<DBImpl*>(db)->GetSuperVersionDebugString() << std::endl;
  std::cout << "block cache usage:"
            << ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache->GetUsage() << std::endl;
  //get final status

  db->Close();
  delete db;
  Status s2 = DB::Open(options, FLAGS_db_name_, &db);
  std::cout << dynamic_cast<DBImpl*>(db)->GetSuperVersionDebugString() << std::endl;
  if (options.dataBlockTypeAdvisor != nullptr) {

    std::cout << options.dataBlockTypeAdvisor->DebugString() << std::endl;
  }
  std::cout << "mem usage:" << getCurrentRSS() / KB / KB << std::endl;
  db->Close();

  delete db;
  delete schema;
  delete bitmap;
}

TEST_F(DBTest, RowColPerformanceTest) {
  std::vector<uint32_t> lengths(FLAGS_col_num_, FLAGS_value_col_length_);
  Schema*schema = GenerateSchema(lengths);
  ASSERT_LE(FLAGS_data_num_,10007*10008);

  int num_records = FLAGS_data_num_;
//  std::vector<std::string> keys;
//  std::vector<std::string> values;
//  GenerateRandomKVs(&keys, &values, 0, num_records, lengths, schema);
  uint32_t n = 10007;
  Random rnd(123);
  random_prefix = RandomStrings(n, 5, &rnd);

  std::string value = GenerateValues(lengths, schema, &rnd);

  ReadOptions readOptionsAll;
  readOptionsAll.SetBitmap(nullptr, schema);
  readOptionsAll.fill_cache = FLAGS_fill_cache_;

  std::vector<TestResult> testResults;

//    std::cout << "Test upon RowGroup with read all columns:" << std::endl;
  TestResult r1 = PerformanceTestBody(schema, DataBlockType::RowGroupBased, readOptionsAll,
                                      FLAGS_db_name_ + (FLAGS_path_with_type_ ? "_col" : ""), num_records, value,
                                      random_prefix, "rowgroup_all", FLAGS_refresh_);
  testResults.emplace_back(std::move(r1));

  std::cout << "==============" << std::endl;

//    std::cout << "Test upon RowGroup with read first column:" << std::endl;
  if (FLAGS_col_num_gap <= 0)
  {
    ReadOptions readOptionsFirstCol;
    char*bitmap = GenerateBitmap(schema->GetColsNum(), {0});
    readOptionsFirstCol.SetBitmap(bitmap, schema);
    readOptionsFirstCol.fill_cache = FLAGS_fill_cache_;
    TestResult r2 = PerformanceTestBody(schema, DataBlockType::RowGroupBased, readOptionsFirstCol,
                                        FLAGS_db_name_ + (FLAGS_path_with_type_ ? "_col" : ""), num_records, value,
                                        random_prefix, "rowgroup_single", false);
    testResults.emplace_back(std::move(r2));
    std::cout << "==============" << std::endl;
    delete bitmap;
  }

  else
  {
      int get_col_num = 1;
      while (get_col_num < (int)schema->GetColsNum()) {
        std::vector<int> select_values;
        for (int i=0; i < get_col_num; i++)
          select_values.push_back(i);
        char*bitmap = GenerateBitmap(schema->GetColsNum(), select_values);
        ReadOptions readOptionsSomeCol;
        readOptionsSomeCol.SetBitmap(bitmap, schema);
        readOptionsSomeCol.fill_cache = FLAGS_fill_cache_;
        TestResult rn = PerformanceTestBody(schema, DataBlockType::RowGroupBased, readOptionsSomeCol,
                                            FLAGS_db_name_ + (FLAGS_path_with_type_ ? "_col" : ""), num_records, value,
                                            random_prefix, "rowgroup_cols_"+std::to_string(get_col_num), false);
        testResults.emplace_back(std::move(rn));
        std::cout << "==============" << std::endl;
        delete bitmap;
        get_col_num += FLAGS_col_num_gap;
      }
  }



//    std::cout << "Test upon Row with read all columns:" << std::endl;
  TestResult r3 = PerformanceTestBody(schema, DataBlockType::RowBased, readOptionsAll,
                                      FLAGS_db_name_ + (FLAGS_path_with_type_ ? "_row" : ""), num_records, value,
                                      random_prefix, "row_all", FLAGS_refresh_);
  std::cout << "==============" << std::endl;

  testResults.emplace_back(std::move(r3));


  std::string summary = option_summary() + getPerformanceResultsSummary(testResults);
  std::cout << summary << std::endl;

  delete schema;
//  delete bitmap;

 csv_rows << FLAGS_STR();

 if (FLAGS_col_num_gap == 0)
 {
   std::fstream csvWriter;
   csvWriter.open(FLAGS_result_file, std::ios::app);
   if(csvWriter.tellp() == 0)
   {
     csv_title << string_format("%9s,%9s,%9s,%9s,%9s,%9s,%9s,%9s,%9s,","RG_PUT","RG_GET_A","RG_SCAN_A","RG_PUT_T","RG_GET_S","RG_SCAN_S","R_PUT","R_GET","R_SCAN") << FLAGS_TITLE();
     csvWriter << csv_title.str() << std::endl;
   }
   csvWriter << csv_rows.str() << std::endl;
   csvWriter.close();
 }

//  std::cout << csv_title.str() << std::endl;
//  std::cout << csv_rows.str() << std::endl;
}




TEST_F(DBTest, bitsetTest) {
  bool b[100];
  char c[100];
  int32_t i[100];
  std::cout << std::endl;
  std::cout << sizeof(b) << std::endl;  //NO OPTIMIZATION
  std::cout << sizeof(c) << std::endl;
  std::cout << sizeof(i) << std::endl;

//  std::bitset<100> bits;
//  std::cout << sizeof(bits) << std::endl;
}

TEST_F(DBTest, stringTest) {
  std::string s;
  s = "3";
  s.reserve(48);
  s.assign("ss",2);
  s.append("aasssss");
  s = "22";

//  s.reserve(10);
  std::cout << s << " " << s.size() << " " << s.capacity() <<  std::endl;
  auto temp = 7 / 5.0;
  std::cout << temp << std::endl;
}


TEST_F(DBTest, BytewiseComparatorTest) {
  auto s = BytewiseComparator();
  Slice a("1000000   0");
  Slice b("100001   0");
  std::cout << s->Compare(a,b) << std::endl;
}

TEST_F(DBTest, VectorTest) {
  std::vector<uint32_t> vec(300000000, 3);
  uint64_t sum = 0;
  uint32_t n = vec.size();
  StopWatchNano timer(Env::Default(), true);


  for (uint32_t i = 0; i < vec.size(); i++) {
    sum += vec[i];
  }
  std::cout << sum << " " << timer.ElapsedNanos(true) << std::endl;

  uint64_t sum2 = 0;
  uint32_t*a = vec.data();
  for (uint32_t i = 0; i < n; i++) {
    sum2 += a[i];
  }
  std::cout << sum2 << " " << timer.ElapsedNanos(true) << std::endl;

  uint64_t sum3 = 0;
  for (uint32_t &s: vec) {
    sum3 += s;
  }
  std::cout << sum3 << " " << timer.ElapsedNanos(true) << std::endl;
}

TestResult AdvisorTestBody(Schema*schema, Options &options, const std::string &kDBName, const ReadOptions &readOptions,
                           const WriteOptions &writeOptions,
                           int start, int end, uint32_t oper_count,
                           const std::vector<std::string> &suffixs, const std::string prefix,
                           const uint32_t oper_kind, Random*rnd, DB*db = nullptr, bool printStat = false) {

//  std::cout << getCurrentRSS() / KB / KB << std::endl;
  TestResult r;
//  DB* db;

  //Options options2 = getDefaultOption(schema);
  if (oper_kind > 0) {
//    options.disable_auto_compactions = true;
  }
  Status s;
if (FLAGS_reopen_each_oper)
{
  s = DB::Open(options, kDBName, &db);
  checkStatus(s);
}


  if (printStat)
    std::cout << GetDBInfo(kDBName,options,db) << std::endl;

//  std::cout << ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache->GetUsage() << std::endl;
  StopWatchNano timer(Env::Default(), true);
  switch (oper_kind)
  {
    case 0:  //put
    {
      r.name = "put" + prefix;
      std::string sampleValue = GenerateSampleValue(schema, rnd);
      std::cout << schema->GetColsNum() << " " << sampleValue.size() << std::endl;
      WriteBatch writeBatch;

      for (int i = start; i < end; ++i) {
        s = writeBatch.Put(GenerateStrWithPrefixAndSuffixes(prefix, i, suffixs), sampleValue);
        checkStatus(s);
        if (writeBatch.GetDataSize() > 4 * MB)
        {
          s = db->Write(writeOptions, &writeBatch);
          checkStatus(s);
          writeBatch.Clear();
        }
      }
      if (writeBatch.GetDataSize() > 0)
      {
        s = db->Write(writeOptions, &writeBatch);
        checkStatus(s);
        writeBatch.Clear();
      }

      break;
    }
    case 1:   //get
    {
      r.name = "get" + prefix;
      auto cfh = db->DefaultColumnFamily();
      int num_records = end - start;
//      int num_get = (end - start) / 10;
      for (uint32_t i = 0; i < oper_count; ++i) {
        std::vector<PinnableSlice> actualValues(schema->GetColsNum());
        s = db->Get(readOptions, cfh,
                    GenerateStrWithPrefixAndSuffixes(prefix, rnd->Uniform(num_records) + start, suffixs),
                    actualValues.data());
        checkStatus(s);
      }
//      dynamic_cast<DBImpl*>(db)->TryConversionAndSchedule(db->DefaultColumnFamily()->GetID());
      break;
    }
    case 2:   //iter scan
    {
      Random new_rnd(100);
//      std::cout << dynamic_cast<DBImpl*>(db)->GetSuperVersionDebugString() << std::endl;
      r.name = "scan" + prefix;
//      std::string to = GenerateStrWithPrefixAndSuffixes(prefix, end, suffixs);
      Iterator* db_iter = db->NewIterator(readOptions);
      if (start <= 0)
        start = 1;
      uint32_t sum = oper_count * 20;

//      for (int rounds =0; rounds < 50; rounds++)
      while (sum > 0)
      {
//        uint32_t count = 0;
        std::string from = GenerateStrWithPrefixAndSuffixes(prefix, new_rnd.Uniform(start), suffixs);
        assert(from.size() == 11);
        for (db_iter->Seek(from); db_iter->Valid() && sum > 0; db_iter->Next(), sum --) {
          Slice key = db_iter->key().ToString();
          if (!key.starts_with(prefix))
            break;
//        if (key[0] != prefix[0])
//          break;
          auto value1 = db_iter->values();
        }
        checkStatus(db_iter->status());
//        sum += count;
      }
      std::cout << sum;
      delete db_iter;

      break;
    }

    default:
      assert(false);
  }
  //async time

  r.result.push_back(timer.ElapsedNanos(false) / pow(10, 9));
  dynamic_cast<DBImpl*>(db)->TryConversionAndSchedule(db->DefaultColumnFamily()->GetID());

  if (FLAGS_reopen_each_oper )
  {
    WaitForDBBackJobsAndClose(db, true);
  }

  if (oper_kind == 0 && !FLAGS_reopen_each_oper )
  {
    //avoid write buffer effect performance
    dynamic_cast<DBImpl*>(db)->Flush(FlushOptions());
    dynamic_cast<DBImpl*>(db)->TEST_WaitForFlushMemTable();
  }

//  if (printdb)
//    std::cout << dynamic_cast<DBImpl*>(db)->GetSuperVersionDebugString() << std::endl;
//  assert(0 == ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache->GetUsage() );
  //including compaction time
  r.result.push_back(timer.ElapsedNanos(true) / pow(10, 9));
  std::cout << string_format("%20s,%10.2f,%10.2f", r.name.c_str(), r.result[0], r.result[1]) << std::endl;

//  std::cout << getCurrentRSS() / KB / KB << std::endl;
  options.disable_auto_compactions = false;
  return r;
}







  // args:small:: --data_num_=2000000 --LO_num_limit_=2 --col_num_=20 --write_buffer_size_=16000000 --block_cache_size_=16000000 --batch_num_=200000 --default_block_type_=1 --converison_=1 --compaction_=1 --max_file_size_=16000000 --level_bytes_base_=64000000 --write_with_read=0
  // args:large:../db_test3 --data_num_=10000000 --col_num_=30  --batch_num_=400000 --default_block_type_=1 --gtest_filter=DBTest.TestAdvisor --write_with_read=0 --converison_=1 --compaction_=1  --LO_num_limit_=2 | unbuffer -p tee col_30.txt
TEST_F(DBTest, TestAdvisor)
{
  std::vector<uint32_t> lengths(FLAGS_col_num_, FLAGS_value_col_length_);
  ASSERT_LE(FLAGS_data_num_,10007*10008);
  Schema* schema = GenerateSchema(lengths);
  Options options = getDefaultOption(schema);
//  ASSERT_TRUE(options.dataBlockTypeAdvisor != nullptr &&
//              (options.dataBlockTypeAdvisor->CompactAdviceEnabled() || options.dataBlockTypeAdvisor->ConversionEnabled())  );

  WriteOptions writeOptions;
  writeOptions.sync = FLAGS_sync_;


  char* bitmap = GenerateBitmap(schema->GetColsNum(), { int(schema->GetColsNum() / 2)});
  std::vector<ReadOptions>  readOptionss;
  readOptionss.emplace_back();
  readOptionss.emplace_back();
  readOptionss[0].SetBitmap(bitmap, schema);
  readOptionss[1].SetBitmap(nullptr,schema);
  readOptionss[0].fill_cache = FLAGS_fill_cache_;
  readOptionss[1].fill_cache = FLAGS_fill_cache_;

//      {ReadOptions(nullptr, schema), ReadOptions(bitmap, schema)};

  ASSERT_EQ(1 ,readOptionss[0].should_read_cnt);
  ASSERT_EQ(schema->GetColsNum() ,readOptionss[1].should_read_cnt);


  Random rnd(123);
  auto random_suffixes = RandomStrings(10007, 5, &rnd); //should not be same to 10008

  int num_records = 0;

  std::vector<std::string> prefixes = {"A","B"};
  std::vector<TestResult> testResults;

  DestroyDB(FLAGS_db_name_, options);
  DB* db = nullptr;
  if (!FLAGS_reopen_each_oper)
  {
      Status s = DB::Open(options, FLAGS_db_name_, &db);
      checkStatus(s);
  }

  StopWatchNano timer(Env::Default(), true);
  if (!FLAGS_write_with_read)
  {
    num_records = 0;
    while (num_records < FLAGS_data_num_) {
      for (uint32_t i = 0; i < prefixes.size(); i++) {
        testResults.emplace_back(
            AdvisorTestBody(schema, options, FLAGS_db_name_, readOptionss[i % readOptionss.size()], writeOptions,
                            num_records, num_records + FLAGS_batch_num_, FLAGS_batch_num_, random_suffixes,
                            prefixes[i], 0, &rnd, db));

      }
      num_records += FLAGS_batch_num_;
      std::cout << num_records << std::endl;
    }
  }
  num_records = 0;
  while (num_records < FLAGS_data_num_) {
    //put
    if (FLAGS_write_with_read)
    {
      for (uint32_t i = 0; i < prefixes.size(); i++) {
        testResults.emplace_back(
            AdvisorTestBody(schema, options, FLAGS_db_name_, readOptionss[i % readOptionss.size()], writeOptions,
                            num_records, num_records + FLAGS_batch_num_, FLAGS_batch_num_, random_suffixes,
                            prefixes[i], 0, &rnd, db));

      }
    }


//    get new record
    for (uint32_t i = 0; i < prefixes.size(); i++) {
      testResults.emplace_back(
          AdvisorTestBody(schema, options, FLAGS_db_name_, readOptionss[i % readOptionss.size()], writeOptions,
                          0, num_records + FLAGS_batch_num_, FLAGS_batch_num_ / 2, random_suffixes,
                          prefixes[i], 1, &rnd, db));
    }

    //TODO check fixed num
    //scan all records
    int k = 5;
    uint32_t random_start = rnd.Uniform(num_records+1);
    for (uint32_t i = 0; i < prefixes.size(); i++) {
      testResults.emplace_back(
          AdvisorTestBody(schema, options, FLAGS_db_name_,
                          readOptionss[i % readOptionss.size()] /* readOptionss[i == 1 ? 0 : i % readOptionss.size()]*/,
                          writeOptions,
                          num_records, num_records + FLAGS_batch_num_, FLAGS_batch_num_ * k, random_suffixes,
                          prefixes[i], 2, &rnd, db, i == 0));
    }

    num_records += FLAGS_batch_num_;
    std::cout << num_records << std::endl;
//    std::cout << GetDBInfo(FLAGS_db_name_,options);

//    std::cout << dynamic_cast<DBImpl*>(db)->GetSuperVersionDebugString() << std::endl;

  }
  if (!FLAGS_reopen_each_oper)
  {
    assert(db != nullptr);
    db->Close();
  }


  auto time = timer.ElapsedNanos(true) / pow(10, 9);
  if (options.dataBlockTypeAdvisor != nullptr)
    std::cout << options.dataBlockTypeAdvisor->DebugString(true) << std::endl;

  std::cout << getAdvisorResultsSummary(testResults) << std::endl;
  std::cout << "total:" << time << std::endl;

}





//only work for old(no value) db
/*
TEST_F(DBTest, CacheOverWithMultiIteratorTest) {
  ReadOptions read_options;
//  read_options.fill_cache = fill_cache;
//  auto table_options = GetTableOptions();
  auto options = getDefaultOption();

  std::shared_ptr<Cache> cache = NewLRUCache(8000, -1, true);
  ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache = cache;

  DestroyDB(FLAGS_db_name_, options);
  DB*db;
  Status s = DB::Open(options, FLAGS_db_name_, &db);

  for (int i = 0; i <= 500; i++)
    db->Put(WriteOptions(), ToString(i), ToString(i));  //each 8 bytes
  db->Close();
  s = DB::Open(options, FLAGS_db_name_, &db);

  std::cout << ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache->GetUsage() << std::endl;
//  ASSERT_EQ(0, ((BlockBasedTableOptions *) options.table_factory->GetOptions())->block_cache->GetUsage());
  auto iter = db->NewIterator(read_options);
  iter->Seek(ToString(0));
  int usage = ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache->GetUsage();
  std::cout << ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache->GetUsage() << std::endl;
//  ASSERT_EQ(usage, ((BlockBasedTableOptions *) options.table_factory->GetOptions())->block_cache->GetUsage());

  auto iter2 = db->NewIterator(read_options);
  iter2->Seek(ToString(500));
  iter2->status();
  ASSERT_TRUE(iter2->Valid());
  std::string value2 = iter2->value().ToString();

  ASSERT_TRUE(iter->Valid());
  std::string value1 = iter->value().ToString();
  std::cout << value1 << " " << value2 << std::endl;
  std::cout << ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache->GetUsage() << std::endl;
//  ASSERT_EQ(fill_cache? usage : 2*usage ,cache->GetUsage() );


  delete iter;
  iter = nullptr;
  std::cout << ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache->GetUsage() << std::endl;
//  ASSERT_EQ(usage, ((BlockBasedTableOptions *) options.table_factory->GetOptions())->block_cache->GetUsage());
  delete iter2;
  std::cout << ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache->GetUsage() << std::endl;
//  ASSERT_EQ(0, cache->GetUsage());

  db->Close();
  delete db;
}
*/


/*
void TestFillCache(bool fill_cache, int capacity) {
  ReadOptions read_options;
  read_options.fill_cache = fill_cache;
//  auto table_options = GetTableOptions();
  auto options = getDefaultOption();

  std::shared_ptr<Cache> cache = NewLRUCache(capacity, -1, false);  //capacity=0 内存中能删就删
  ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache = cache;

  DestroyDB(FLAGS_db_name_, options);
  DB*db;
  Status s = DB::Open(options, FLAGS_db_name_, &db);

  for (int i = 0; i < 100; i++)
    db->Put(WriteOptions(), ToString(i), ToString(i));
  db->Close();
  s = DB::Open(options, FLAGS_db_name_, &db);

  std::cout << ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache->GetUsage() << std::endl;
  ASSERT_EQ(0, ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache->GetUsage());
  auto iter = db->NewIterator(read_options);
  iter->Seek(ToString(0));
  int usage = ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache->GetUsage();
  std::cout << ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache->GetUsage() << std::endl;
  ASSERT_EQ(usage, ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache->GetUsage());

  auto iter2 = db->NewIterator(read_options);
  iter2->Seek(ToString(0));
  std::cout << ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache->GetUsage() << std::endl;
  ASSERT_EQ(fill_cache ? usage : 2 * usage, cache->GetUsage());

  delete iter;
  iter = nullptr;
  std::cout << ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache->GetUsage() << std::endl;
  ASSERT_EQ(usage, ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache->GetUsage());
  delete iter2;
  std::cout << ((BlockBasedTableOptions*) options.table_factory->GetOptions())->block_cache->GetUsage() << std::endl;
  ASSERT_EQ(capacity > 6000 ? usage : 0, cache->GetUsage());

  db->Close();
  delete db;
}

 TEST_F(DBTest, DBFillCacheTest) {
  TestFillCache(false, 0);
  TestFillCache(true, 0);
  TestFillCache(true, 8000);
}
 */

//TEST_F(DBTest, testInt3264Performance)
//{
//  StopWatchNano timer(Env::Default(), true);
//
//  size_t sum2 = 0;
//  for (uint32_t i = 0; i < 10000000; i++) {
//    sum2 = sum2 + i;
//  }
//  std::cout << sum2 << " " << timer.ElapsedNanos(true) << std::endl;
//
//  uint64_t sum = 0;
//  for (uint64_t i = 0; i < 10000000; i++) {
//    sum = sum + i;
//  }
//  std::cout << sum << " " << timer.ElapsedNanos(true) << std::endl;
//
//
//
//  float sum3 = 0;
//  for (uint32_t i = 0; i < 10000000; i++) {
//    sum3 = sum3 + i;
//  }
//  std::cout << sum3 << " " << timer.ElapsedNanos(true) << std::endl;
//
//}


}  // namespace rocksdb

int main(int argc, char**argv) {
  rocksdb::port::InstallStackTraceHandler();
  ::testing::InitGoogleTest(&argc, argv);

  GFLAGS_NAMESPACE::ParseCommandLineFlags(&argc, &argv, true);


  return RUN_ALL_TESTS();
}
