#include "my_test.h"
#include "util/comparator.cc"
namespace rocksdb {

class HTAP_TEST : public testing::Test{};

class LittleEndianBytesForIntComparator : BytewiseComparatorImpl {
public:
  int Compare(const Slice &a, const Slice &b) const override {
    if (a.size() == b.size())
    {
      auto* ua = (const unsigned char*)(a.data());
      auto* ub = (const unsigned char*)(b.data());

      size_t i = a.size()-1;
      for (; i > 0 && ua[i] == ub[i]; i--);

      return ua[i] > ub[i] ? 1 :
             (ua[i] < ub[i] ? -1 : 0);
    }
    else return a.size() > b.size() ? 1 : -1;
  }



};

//int select_col_num = FLAGS_col_num_ * FLAGS_col_frac;
//std::vector<int> select_values;
// QIWANG 10 30 80


TestResult HTAPTestBody(DB*db, Schema*schema, Options &options, Random*rnd, char query_id, const ReadOptions& readOptions, int select_col_num) {
  TestResult r;
  StopWatchNano timer(Env::Default(), true);
//  options.comparator = Compar
  int rounds = query_id == '0' ? 1 : FLAGS_HTAP_query_rounds;

  for (int t = 0; t < rounds; t++)
  {
    switch (query_id) {
      case '0': // Q0: INSERT INTO R VALUES (a0,a1,...,ap)
      {
        //insert FLAGS_data_num_ records
        WriteOptions writeOptions;
        writeOptions.sync = FLAGS_sync_;
        WriteBatch writeBatch;
        std::vector<uint32_t> values_int(FLAGS_col_num_);  //for temproary buffer
        for (int i = 0; i < FLAGS_data_num_; i++) {
          uint32_t key_int = rnd->Uniform(FLAGS_HTAP_MAX_KEY);
          Slice key_slice(static_cast<char*>(static_cast<void*>(&key_int)),
                          sizeof(key_int));  //INT TO BYTES, works on linux and macos
          std::vector<Slice> values_slice;
          values_slice.reserve(FLAGS_col_num_);
          for (int j = 0; j < FLAGS_col_num_; j++) {
            values_int[j] = (key_int % 10000 + j)  * j % 10000;
            values_slice.emplace_back(static_cast<char*>(static_cast<void*>(&values_int[j])),
                                      sizeof(values_int[j])); //INT TO BYTES
          }
          std::string merged_value;
          schema->BuildValueSlice(values_slice.data(), &merged_value, nullptr);
          writeBatch.Put(key_slice, merged_value);
        }
        db->Write(writeOptions, &writeBatch);
      }

      case '1': // SELECT COUNT FROM R Where a0<δ
      {
        int border = FLAGS_HTAP_MAX_KEY * FLAGS_num_frac;
        int count = 0;
        Iterator*db_iter = db->NewIterator(readOptions);
        for (db_iter->SeekToFirst(); db_iter->Valid(); db_iter->Next()) {
          const int key_int = *(static_cast<const int*>(static_cast<const void*>(db_iter->key().data())));
          if (key_int >= border)
            break;
          count++;
        }
        checkStatus(db_iter->status());
        delete db_iter;
        break;
      }

      case '2': //Q1: SELECT a1,a2,. . .,ak FROM R WHERE a0 < δ
      {
        //init read
        int border = FLAGS_HTAP_MAX_KEY * FLAGS_num_frac;
        Iterator*db_iter = db->NewIterator(readOptions);

        for (db_iter->SeekToFirst(); db_iter->Valid(); db_iter->Next()) {
          const int key_int = *(static_cast<const int*>(static_cast<const void*>(db_iter->key().data())));
          if (key_int >= border)
            break;
          auto values = db_iter->values();
          if (select_col_num > 0)
            check(values[select_col_num - 1].size() == 4, "values[select_col_num-1].size() == 4");
        }
        checkStatus(db_iter->status());
        delete db_iter;
        break;
      }
      case '3': // SELECT MAX(a1),...,MAX(ak) FROM R WHERE a0 <δ   aggregate
      {
        int border = FLAGS_HTAP_MAX_KEY * FLAGS_num_frac;
        Iterator*db_iter = db->NewIterator(readOptions);

        std::vector<int> maxs(select_col_num);
        for (db_iter->SeekToFirst(); db_iter->Valid(); db_iter->Next()) {
          const int key_int = *(static_cast<const int*>(static_cast<const void*>(db_iter->key().data())));
          if (key_int >= border)
            break;
          auto values = db_iter->values();

          for (int i = 0; i < select_col_num; i++) {
            const int value_int = *(static_cast<const int*>(static_cast<const void*>(values[i].data())));
            if (value_int > maxs[i])
              maxs[i] = value_int;
          }
        }
        checkStatus(db_iter->status());
        delete db_iter;
        break;
      }
      case '4': //JOIN TODO
      {
        break;
      }
      default:
        assert(false);
    }
  }

  r.name = query_id;
  r.result.push_back(timer.ElapsedNanos(true) / pow(10, 9));
  return r;
}

TEST_F(HTAP_TEST, TestHTAP)
//TEST 4个百分比
{
//init db
  std::vector<uint32_t> lengths(FLAGS_col_num_, FLAGS_value_col_length_);
  ASSERT_LE(FLAGS_data_num_ * FLAGS_HTAP_rounds, 10007 * 10008);
  Schema*schema = GenerateSchema(lengths);
  Options options = getDefaultOption(schema);
  shared_ptr<Comparator> cmp ((Comparator*) new LittleEndianBytesForIntComparator());
  options.comparator = cmp.get();


  if (FLAGS_refresh_) {
    DestroyDB(FLAGS_db_name_, options);
  }
  DB*db;
  Status s = DB::Open(options, FLAGS_db_name_, &db);
  ASSERT_OK(s);

  //INIT ReadOption
  ReadOptions readOptions;
  int select_col_num = FLAGS_col_num_  > FLAGS_col_get ? FLAGS_col_get : FLAGS_col_num_;
  std::vector<int> select_values;
  for (int i = 0; i < select_col_num; i++)
    select_values.push_back(i);
  shared_ptr<char> bitmap(GenerateBitmap(FLAGS_col_num_, select_values));
  readOptions.SetBitmap(bitmap.get(), schema);
  readOptions.fill_cache = FLAGS_fill_cache_;

  //fstream
  std::ofstream htap_advisor_out("htap_advisor_out.text");

//queries list
  std::vector<char> query_ids;
  for (char c : FLAGS_htap_workloads)
    if (c >= '0' && c <= '9') {
      query_ids.emplace_back(c);
    }
  Random rnd(100);
  StopWatchNano timer(Env::Default(), true);
  std::vector<float> round_times;
  round_times.push_back(0);
  for (int i = 0; i < FLAGS_HTAP_rounds; i++) {
    for (char query_id : query_ids) {

      TestResult r = HTAPTestBody(db, schema, options, &rnd, query_id, readOptions, select_col_num);
      dynamic_cast<DBImpl*>(db)->TryConversionAndSchedule(db->DefaultColumnFamily()->GetID());

      std::cout << string_format("%20s,%10.2f", r.name.c_str(), r.result[0]) << std::endl;
      htap_advisor_out << "event: " << query_id << std::endl;
      htap_advisor_out << dynamic_cast<DBImpl*>(db)->GetSuperVersionDebugString(true) << std::endl;

    }
    round_times.push_back(timer.ElapsedNanos(false) / pow(10,9 ));
  }
  std::cout << "summary,";

  std::cout << string_format("%.2f,", float(dynamic_cast<DBImpl*>(db)->GetDBSize()) / float(MB));  //dbsize
  std::cout << string_format("%.2f,", round_times.back() - round_times[2]); //4 - 10
  std::cout << string_format("%.2f,", timer.ElapsedNanos(true) / pow(10, 9));  // 1 - 10
  for (int i=1; i <= FLAGS_HTAP_rounds; i ++)
  {
    std::cout << string_format("%.2f,", round_times[i] - round_times[i-1]); //each rounds
  }
  std::cout << std::endl;
  std::cout
      << string_format("d:%d,c:%d,fd:%.2f,fc:%d", FLAGS_data_num_, FLAGS_col_num_, FLAGS_num_frac, FLAGS_col_get);

  WaitForDBBackJobsAndClose(db,true);
//  delete db;
}

TEST_F(HTAP_TEST, testLittleEndian) {
  int a = 0x01;
  int b = 0x0100;
  int c = 128;
  int d = 65407;
  LittleEndianBytesForIntComparator littleEndianBytesForIntComparator;
//  unsigned char* temp = ;
  Slice a_slice(static_cast<char*>(static_cast<void*>(&a)), sizeof(a));
  Slice b_slice(static_cast<char*>(static_cast<void*>(&b)), sizeof(b));
  Slice c_slice(static_cast<char*>(static_cast<void*>(&c)), sizeof(c));
  Slice d_slice(static_cast<char*>(static_cast<void*>(&d)), sizeof(d));

  ASSERT_TRUE(a_slice.compare(b_slice) > 0);
  ASSERT_TRUE(littleEndianBytesForIntComparator.Compare(a_slice,b_slice) < 0);
  ASSERT_TRUE(littleEndianBytesForIntComparator.Compare(c_slice,d_slice) < 0);

}

}

int main(int argc, char**argv) {
  rocksdb::port::InstallStackTraceHandler();
  ::testing::InitGoogleTest(&argc, argv);

  GFLAGS_NAMESPACE::ParseCommandLineFlags(&argc, &argv, true);


  return RUN_ALL_TESTS();
}